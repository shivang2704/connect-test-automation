package org.zomentum.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.zomentum.base.BasePage;
import org.zomentum.utils.PropertyReader;
import org.zomentum.utils.TOTPGenerator;

public class LoginPage extends BasePage {

    WebDriver driver;

    public LoginPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }
    public static PropertyReader props = new PropertyReader();

    public static String url = props.readItem("loginUrlConnect");
    public static String userName = props.readItem("userNameConnect");
    public static String password = props.readItem("passwordConnect");
    public static String userNameZomentum = props.readItem("zomentumUserName");
    public static String passwordZomentum = props.readItem("passwordZomentum");

    //Page objects
    By userNameInput = By.id("username");
    By passwordInput = By.id("password");
    By continueButton = By.xpath("//button[@type='submit' and text()='Continue']");
    By contractsMenuItem = By.xpath("//span[.='Contracts']");

    By googleWorkspaceGoolash = By.xpath("//div[@class='AuthBoxRow--name']");
    By gmailUsernameInput = By.id("identifierId");
    By emailNextBtn = By.xpath("//span[text()='Next']");
    By gmailPasswordInput = By.xpath("//input[@type='password']");
    By anotherWayBtn = By.xpath("//span[text()='Try another way']");
    By googleAuthenticatorBtn = By.xpath("//div/strong[contains(text(),'Google Authenticator')]//parent::div");
    By tOTPPinTextBox = By.id("totpPin");



    //Page Actions
    public void openLoginPage(){
        gotoURL(url);
    }

    public void loginToGoolash() throws InterruptedException {
        waitForElementToBeClickable(googleWorkspaceGoolash);
        click(googleWorkspaceGoolash);
        waitForElementToBeClickable(gmailUsernameInput);
        click(gmailUsernameInput);
        writeText(gmailUsernameInput,userNameZomentum);
        waitForElementToBeClickable(emailNextBtn);
        click(emailNextBtn);
        waitForElementToBeClickable(gmailPasswordInput);
        click(gmailPasswordInput);
        writeText(gmailPasswordInput,passwordZomentum);
        waitForElementToBeClickable(emailNextBtn);
        click(emailNextBtn);
        waitForElementToBeClickable(anotherWayBtn);
        makeElementVisible(anotherWayBtn);
        click(anotherWayBtn);
        waitForElementToAppear(googleAuthenticatorBtn);
        waitForElementToBeClickable(googleAuthenticatorBtn);
        click(googleAuthenticatorBtn);
        waitForElementToAppear(tOTPPinTextBox);
        waitForElementToBeClickable(tOTPPinTextBox);
        //click(tOTPPinTextBox);
        writeText(tOTPPinTextBox, TOTPGenerator.getTwoFactorCode());
        waitForElementToBeClickable(emailNextBtn);
        makeElementVisible(emailNextBtn);
        click(emailNextBtn);
    }

    public void loginToConnect(){
        waitForElementToBeClickable(userNameInput);
        writeText(userNameInput,userName);
        writeText(passwordInput,password);
        waitForElementToBeClickable(continueButton);
        click(continueButton);
        waitForElementToAppear(contractsMenuItem);
    }

    public void createLoginSessionAndVerify() throws InterruptedException {
        openLoginPage();
        loginToGoolash();
        loginToConnect();
    }
}

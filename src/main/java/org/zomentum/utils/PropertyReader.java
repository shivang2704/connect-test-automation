package org.zomentum.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyReader {

    static Properties properties;

    public PropertyReader(){
        loadAllProperties();
    }

    public void loadAllProperties(){
        properties = new Properties();
        try{
            //String env = System.getProperty("env");
            //System.out.println(env);
            //String filePath = System.getProperty("user.dir")+"/src/main/resources/config_"+ env + ".properties";
            String filePath = System.getProperty("user.dir")+"/src/main/resources/config_staging.properties";
            properties.load(new FileInputStream(filePath));

        }catch(IOException e){
            throw new RuntimeException("Not able to find the properties file");
        }
    }

    public static String readItem(String propertyName){
        return properties.getProperty(propertyName);
    }
}

package org.zomentum.base;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.zomentum.utils.WaitForHelper;

import java.util.Iterator;
import java.util.Set;

public class BasePage {
    public WebDriver driver;

    public BasePage (WebDriver driver){
        this.driver = driver;
    }

    public void gotoURL(String url) {
        driver.get(url);
    }

    public void waitForElementToAppear(By elementLocation) {
        new WaitForHelper(driver).presenceOfTheElement(elementLocation);
    }

    public void waitForElementToBeClickable(By elementLocation){
        new WaitForHelper(driver).elementToBeClickable(elementLocation);
    }

    public void waitForTime() {
        new WaitForHelper(driver).implicitWait( );
    }

    public void click(By elementLocation) {
        driver.findElement(elementLocation).click( );
    }

    public void rightClick(By elementLocation){
        new Actions(driver).contextClick(driver.findElement(elementLocation)).perform();
    }

    public void writeText(By elementLocation, String text) {
        driver.findElement(elementLocation).clear( );
        driver.findElement(elementLocation).sendKeys(text);
    }

    public void clearText(By elementLocation){
        driver.findElement(elementLocation).clear( );
        driver.findElement(elementLocation).sendKeys(Keys.SHIFT, Keys.ARROW_UP);
        driver.findElement(elementLocation).sendKeys(Keys.DELETE);
    }

    public void clickDeleteBtn(By elementLocation){
        driver.findElement(elementLocation).click( );
        driver.findElement(elementLocation).sendKeys(Keys.BACK_SPACE);
    }

    public void pressEnter(By elementLocation){
        driver.findElement(elementLocation).sendKeys(Keys.RETURN);
    }

    public void pressDownKeyAndEnter(By elementLocation){
        driver.findElement(elementLocation).sendKeys(Keys.ARROW_DOWN,Keys.RETURN);
    }

    public String readText(By elementLocation) {
        return driver.findElement(elementLocation).getText( );
    }

    public void moveToElement(By elementLocation) {
        new Actions(driver).moveToElement(driver.findElement(elementLocation)).build( ).perform( );
    }

    public boolean elementIsDisplayed(By elementLocation){
        boolean status;
        if(driver.findElements(elementLocation).size() != 0){
            status = true;
        }
        else status = false;
        return status;
    }

    public void makeElementVisible(By elementLocation) throws InterruptedException {
        WebElement e = driver.findElement(elementLocation);
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView({block: 'center', inline: 'nearest'});", e);
        hardWait(500);
    }

    public void clickByJs(By elementLocation){
        WebElement e = driver.findElement(elementLocation);
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();", e);
    }

    public void hardWait(int time) throws InterruptedException {
        Thread.sleep(time);
    }

    public void waitForElementToBeInvisible(By elementLocation){
        new WaitForHelper(driver).invisibilityOfElement(elementLocation);
    }

    public void selectFromDDByValue(By elementLocation, String value){
        Select se = new Select(driver.findElement(elementLocation));
        se.selectByValue(value);
    }

}

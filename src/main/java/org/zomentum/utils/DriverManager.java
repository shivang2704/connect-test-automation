package org.zomentum.utils;

import org.openqa.selenium.WebDriver;

public class DriverManager {
    public static ThreadLocal<WebDriver> driver = new ThreadLocal<>();
    //WebDriver driver;
    public WebDriver getDriver(){
        return driver.get();
    }
}

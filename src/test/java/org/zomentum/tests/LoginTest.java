package org.zomentum.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.zomentum.base.BaseTest;
import org.zomentum.pages.LoginPage;
import org.zomentum.utils.TOTPGenerator;

public class LoginTest extends BaseTest {


    @Test
    public void loginToConnect(){
        LoginPage loginPage = new LoginPage(getDriver());
        try {
            loginPage.createLoginSessionAndVerify();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}

# Zomentum Connect Test

Project for testing Zomentum API

## Getting started

1. Clone the repository
2. Get Java JDK version 17.0.2
3. Import the project directory to IntelliJ IDEA
4. Sync pom.xml (maven) to resolve library dependencies all other prequisites will be installed by this
5. Go to src/test/java/org.zomentum/tests
6. Open a test class there are test cases in the classes
7. To run a particular single test run that or run the complete class to execute all the tests in that class

## To run from command line

1. Go to project loctaion
2. Open terminal/cmd in the root folder of project
3. Type "mvn clean test" to run all test on default env i.e. staging
4. Type "mvn clean test -D test=class_name" to run the tests under a particular class for eg . "mvn clean test -D test=ImportTest"
5. Type "mvn clean test -D test=class_name#methodName" to run a single test under a particular class for eg. "mvn clean test -D test=ImportTest#verifyImportCSVNewLicenseBasedVendor"

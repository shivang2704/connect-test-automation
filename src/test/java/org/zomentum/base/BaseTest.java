package org.zomentum.base;

import org.openqa.selenium.WebDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.grid.Main;
import org.testng.annotations.*;
import org.zomentum.utils.DriverManager;
import org.zomentum.utils.Log;
import org.zomentum.utils.PropertyReader;

public class BaseTest extends DriverManager {
    //WebDriver driver;
    PropertyReader prop = new PropertyReader();

    //public BaseTest(){
    //    this.driver = super.getDriver();
    //    Log.info("Getting WebDriver Settings");
    //}

    @BeforeMethod(alwaysRun = true)
    public void setup(){
        try{
            if(prop.readItem("browser").equalsIgnoreCase("chrome")){
                Log.info("Found chrome as Browser");
                WebDriverManager.chromedriver().setup();
                ChromeOptions options = new ChromeOptions();
                //options.addArguments("--kiosk");
                //options.setHeadless(true);
                driver.set(new ChromeDriver(options));
                getDriver().manage().window().maximize();
                getDriver().manage().deleteAllCookies();
            }

            else if (prop.readItem("browser").equalsIgnoreCase("firefox")) {
                Log.info("Found firefox as Browser");
                System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+"/geckodriver");
                driver.set(new FirefoxDriver( ));
                getDriver().manage( ).window( ).maximize( );
                getDriver().manage().deleteAllCookies();
            }

            else{
                try{
                    throw new Exception(" Browser driver is not supported.");
                } catch (Exception e) {
                    Log.error("No compatible browser found", e);
                }
            }
        }
        catch(Exception e){
            Log.error("Browser Launch error", e);
        }
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(){
        getDriver().quit();
    }
}

package org.zomentum.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import org.zomentum.base.BaseTest;
import org.zomentum.pages.ConfigurePage;
import org.zomentum.pages.ImportPage;
import org.zomentum.pages.LoginPage;
import org.zomentum.utils.PropertyReader;

import java.io.File;
import java.util.Date;

import java.text.SimpleDateFormat;

public class ImportTest extends BaseTest {

    public static PropertyReader props = new PropertyReader();
    public static String csv1LicenseName = props.readItem("csv1LicenseBasedVendor");
    public static String csv2LicenseName = props.readItem("csv2LicenseBasedVendor");
    public static String csv3LicenseName = props.readItem("csv3LicenseBasedVendor");
    public static String csv4LicenseName = props.readItem("csv4LicenseBasedVendor");
    public static String csv1ConsumptionName = props.readItem("csv1ConsumptionBasedVendor");
    public static String csv2ConsumptionName = props.readItem("csv2ConsumptionBasedVendor");
    public static String csv3ConsumptionName = props.readItem("csv3ConsumptionBasedVendor");
    public static String csv4ConsumptionName = props.readItem("csv4ConsumptionBasedVendor");
    public static String csv5ConsumptionName = props.readItem("csv5ConsumptionBasedVendor");

    public void loginToConnect(){
        LoginPage loginPage = new LoginPage(getDriver());
        try {
            loginPage.createLoginSessionAndVerify();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void verifyImportCSVNewLicenseBasedVendor(){
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String vendorName = timeStamp+" Vendor";
        String currentUserDir = System.getProperty("user.dir");
        String csv1License = currentUserDir + File.separator + csv1LicenseName;
        SoftAssert softAssert = new SoftAssert();
        loginToConnect();
        ImportPage importPage = new ImportPage(getDriver());
        importPage.openImportsPage();
        try {
            importPage.importDataForNewVendorLicenseBased(csv1License,vendorName);
            importPage.verifyImportedData();
            String licenseRow1 = importPage.contractLicenseRow1();
            softAssert.assertEquals(licenseRow1,"10","Vendor in row 1 data is not as expected");
            String licenseRow2 = importPage.contractLicenseRow2();
            softAssert.assertEquals(licenseRow2,"20","Vendor in row 2 data is not as expected");
            String licenseRow3 = importPage.contractLicenseRow3();
            softAssert.assertEquals(licenseRow3,"30","Vendor in row 3 data is not as expected");
            String licenseRow4 = importPage.contractLicenseRow4();
            softAssert.assertEquals(licenseRow4,"40","Vendor in row 4 data is not as expected");
            String licenseRow5 = importPage.contractLicenseRow5();
            softAssert.assertEquals(licenseRow5,"50","Vendor in row 5 data is not as expected");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        ConfigurePage configurePage = new ConfigurePage(getDriver());
        configurePage.openConfigurePage();
        configurePage.openCSVVendorsTab();
        try {
            boolean result = configurePage.deleteCSVVendor(vendorName);
            softAssert.assertFalse(result,"Vendor is not deleted");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        softAssert.assertAll();
    }

    @Test
    public void verifyUpdateCSVLicenseBased(){
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String vendorName = timeStamp+" Vendor";
        String currentUserDir = System.getProperty("user.dir");
        String csv1License = currentUserDir + File.separator + csv1LicenseName;
        String csv2License = currentUserDir + File.separator + csv2LicenseName;
        SoftAssert softAssert = new SoftAssert();
        loginToConnect();
        ImportPage importPage = new ImportPage(getDriver());
        importPage.openImportsPage();
        try {
            importPage.importDataForNewVendorLicenseBased(csv1License,vendorName);
            importPage.verifyImportedData();
            String licenseRow1 = importPage.contractLicenseRow1();
            softAssert.assertEquals(licenseRow1,"10","Vendor in row 1 data is not as expected");
            String licenseRow2 = importPage.contractLicenseRow2();
            softAssert.assertEquals(licenseRow2,"20","Vendor in row 2 data is not as expected");
            String licenseRow3 = importPage.contractLicenseRow3();
            softAssert.assertEquals(licenseRow3,"30","Vendor in row 3 data is not as expected");
            String licenseRow4 = importPage.contractLicenseRow4();
            softAssert.assertEquals(licenseRow4,"40","Vendor in row 4 data is not as expected");
            String licenseRow5 = importPage.contractLicenseRow5();
            softAssert.assertEquals(licenseRow5,"50","Vendor in row 5 data is not as expected");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        importPage.openImportsPage();
        try {
            importPage.updateImportedCSV(csv2License,vendorName);
            importPage.verifyImportedData();
            String licenseRow1 = importPage.contractLicenseRow1();
            softAssert.assertEquals(licenseRow1,"50","Vendor in row 1 data is not as expected");
            String licenseRow2 = importPage.contractLicenseRow2();
            softAssert.assertEquals(licenseRow2,"40","Vendor in row 2 data is not as expected");
            String licenseRow3 = importPage.contractLicenseRow3();
            softAssert.assertEquals(licenseRow3,"30","Vendor in row 3 data is not as expected");
            String licenseRow4 = importPage.contractLicenseRow4();
            softAssert.assertEquals(licenseRow4,"20","Vendor in row 4 data is not as expected");
            String licenseRow5 = importPage.contractLicenseRow5();
            softAssert.assertEquals(licenseRow5,"10","Vendor in row 5 data is not as expected");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        ConfigurePage configurePage = new ConfigurePage(getDriver());
        configurePage.openConfigurePage();
        configurePage.openCSVVendorsTab();
        try {
            boolean result = configurePage.deleteCSVVendor(vendorName);
            softAssert.assertFalse(result,"Vendor is not deleted");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        softAssert.assertAll();
    }

    @Test
    public void verifyLicenseCountRoundOffImport(){
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String vendorName = timeStamp+" Vendor";
        String currentUserDir = System.getProperty("user.dir");
        String csv1License = currentUserDir + File.separator + csv3LicenseName;
        SoftAssert softAssert = new SoftAssert();
        loginToConnect();
        ImportPage importPage = new ImportPage(getDriver());
        importPage.openImportsPage();
        try {
            importPage.importDataForNewVendorLicenseBased(csv1License,vendorName);
            importPage.verifyImportedData();
            String licenseRow1 = importPage.contractLicenseRow1();
            softAssert.assertEquals(licenseRow1,"10","Vendor in row 1 data is not as expected");
            String licenseRow2 = importPage.contractLicenseRow2();
            softAssert.assertEquals(licenseRow2,"20","Vendor in row 2 data is not as expected");
            String licenseRow3 = importPage.contractLicenseRow3();
            softAssert.assertEquals(licenseRow3,"30","Vendor in row 3 data is not as expected");
            String licenseRow4 = importPage.contractLicenseRow4();
            softAssert.assertEquals(licenseRow4,"40","Vendor in row 4 data is not as expected");
            String licenseRow5 = importPage.contractLicenseRow5();
            softAssert.assertEquals(licenseRow5,"50","Vendor in row 5 data is not as expected");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        ConfigurePage configurePage = new ConfigurePage(getDriver());
        configurePage.openConfigurePage();
        configurePage.openCSVVendorsTab();
        try {
            boolean result = configurePage.deleteCSVVendor(vendorName);
            Assert.assertFalse(result,"Vendor is not deleted");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        softAssert.assertAll();
    }

    @Test
    public void verifyLicenseNonRoundOffImport(){
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String vendorName = timeStamp+" Vendor";
        String currentUserDir = System.getProperty("user.dir");
        String csv3License = currentUserDir + File.separator + csv3LicenseName;
        SoftAssert softAssert = new SoftAssert();
        loginToConnect();
        ImportPage importPage = new ImportPage(getDriver());
        importPage.openImportsPage();
        try {
            boolean result = importPage.importDataForNonRoundOffLicense(csv3License,vendorName);
            softAssert.assertTrue(result,"License Count Checkbox is not displayed for decimal column");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        softAssert.assertAll();
    }

    @Test
    public void verifyChangeDefaultColumnValues(){
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String vendorName = timeStamp+" Vendor";
        String currentUserDir = System.getProperty("user.dir");
        String csv1License = currentUserDir + File.separator + csv4LicenseName;
        SoftAssert softAssert = new SoftAssert();
        loginToConnect();
        ImportPage importPage = new ImportPage(getDriver());
        importPage.openImportsPage();
        importPage.selectImportFile(csv1License);
        importPage.createVendorLicenseBased(vendorName);
        importPage.clickImportDataBtn();
        try {
            importPage.openConfigureDefaultColumnValue();
            importPage.changeCustomerNameDefaultValue("Default Customer Name");
            importPage.changeProductNameDefaultValue("Default Product Name");
            importPage.mapData2();
            importPage.clickPreviewImportBtn();
            importPage.clickImportDataBtn();
            importPage.verifyImportedData();
            String licenseRow1 = importPage.contractLicenseRow1();
            softAssert.assertEquals(licenseRow1,"10","Vendor in row 1 data is not as expected");
            String licenseRow2 = importPage.contractLicenseRow2();
            softAssert.assertEquals(licenseRow2,"20","Vendor in row 2 data is not as expected");
            String licenseRow3 = importPage.contractLicenseRow3();
            softAssert.assertEquals(licenseRow3,"30","Vendor in row 3 data is not as expected");
            String licenseRow4 = importPage.contractLicenseRow4();
            softAssert.assertEquals(licenseRow4,"40","Vendor in row 4 data is not as expected");
            String licenseRow5 = importPage.contractLicenseRow5();
            softAssert.assertEquals(licenseRow5,"50","Vendor in row 5 data is not as expected");
            String customerRow1 = importPage.getCustomerNameFromRow("1");
            String customerRow2 = importPage.getCustomerNameFromRow("2");
            String customerRow3 = importPage.getCustomerNameFromRow("3");
            String customerRow4 = importPage.getCustomerNameFromRow("4");
            String customerRow5 = importPage.getCustomerNameFromRow("5");
            softAssert.assertEquals(customerRow1,"Default Customer Name","Customer Name in row 1 data is not as expected");
            softAssert.assertEquals(customerRow2,"Default Customer Name","Customer Name in row 2 data is not as expected");
            softAssert.assertEquals(customerRow3,"Default Customer Name","Customer Name in row 3 data is not as expected");
            softAssert.assertEquals(customerRow4,"Default Customer Name","Customer Name in row 4 data is not as expected");
            softAssert.assertEquals(customerRow5,"Default Customer Name","Customer Name in row 5 data is not as expected");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        ConfigurePage configurePage = new ConfigurePage(getDriver());
        configurePage.openConfigurePage();
        configurePage.openCSVVendorsTab();
        try {
            boolean result = configurePage.deleteCSVVendor(vendorName);
            softAssert.assertFalse(result,"Vendor is not deleted");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        softAssert.assertAll();
    }

    @Test
    public void verifyImportCSVNewConsumptionBasedVendor(){
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String vendorName = timeStamp+" Vendor";
        String currentUserDir = System.getProperty("user.dir");
        String csv1Consumption = currentUserDir + File.separator + csv1ConsumptionName;
        SoftAssert softAssert = new SoftAssert();
        loginToConnect();
        ImportPage importPage = new ImportPage(getDriver());
        importPage.openImportsPage();
        try {
            importPage.importDataForNewVendorConsumptionBased(csv1Consumption,vendorName);
            importPage.verifyImportedDataConsumption();
            String customerRow1 = importPage.getCustomerNameConsumptionFromRow("1");
            String customerRow2 = importPage.getCustomerNameConsumptionFromRow("2");
            String customerRow3 = importPage.getCustomerNameConsumptionFromRow("3");
            String customerRow4 = importPage.getCustomerNameConsumptionFromRow("4");
            String customerRow5 = importPage.getCustomerNameConsumptionFromRow("5");
            softAssert.assertTrue(customerRow1.contains("Test 1"),"Customer Name in row 1 data is not as expected");
            softAssert.assertTrue(customerRow2.contains("Test 2"),"Customer Name in row 2 data is not as expected");
            softAssert.assertTrue(customerRow3.contains("Test 3"),"Customer Name in row 3 data is not as expected");
            softAssert.assertTrue(customerRow4.contains("Test 4"),"Customer Name in row 4 data is not as expected");
            softAssert.assertTrue(customerRow5.contains("Test 5"),"Customer Name in row 5 data is not as expected");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        ConfigurePage configurePage = new ConfigurePage(getDriver());
        configurePage.openConfigurePage();
        configurePage.openCSVVendorsTab();
        try {
            boolean result = configurePage.deleteCSVVendor(vendorName);
            softAssert.assertFalse(result,"Vendor is not deleted");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        softAssert.assertAll();
    }

    @Test
    public void verifyUpdateCSVConsumptionBasedVendor(){
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String vendorName = timeStamp+" Vendor";
        String currentUserDir = System.getProperty("user.dir");
        String csv1Consumption = currentUserDir + File.separator + csv1ConsumptionName;
        String csv2Consumption = currentUserDir + File.separator + csv2ConsumptionName;
        SoftAssert softAssert = new SoftAssert();
        loginToConnect();
        ImportPage importPage = new ImportPage(getDriver());
        importPage.openImportsPage();
        try {
            importPage.importDataForNewVendorConsumptionBased(csv1Consumption,vendorName);
            importPage.verifyImportedDataConsumption();
            String customerRow1 = importPage.getCustomerNameConsumptionFromRow("1");
            String customerRow2 = importPage.getCustomerNameConsumptionFromRow("2");
            String customerRow3 = importPage.getCustomerNameConsumptionFromRow("3");
            String customerRow4 = importPage.getCustomerNameConsumptionFromRow("4");
            String customerRow5 = importPage.getCustomerNameConsumptionFromRow("5");
            softAssert.assertTrue(customerRow1.contains("Test 1"),"Customer Name in row 1 data is not as expected");
            softAssert.assertTrue(customerRow2.contains("Test 2"),"Customer Name in row 2 data is not as expected");
            softAssert.assertTrue(customerRow3.contains("Test 3"),"Customer Name in row 3 data is not as expected");
            softAssert.assertTrue(customerRow4.contains("Test 4"),"Customer Name in row 4 data is not as expected");
            softAssert.assertTrue(customerRow5.contains("Test 5"),"Customer Name in row 5 data is not as expected");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        importPage.openImportsPage();
        try {
            importPage.updateImportedCSVConsumption(csv2Consumption,vendorName);
            importPage.verifyImportedDataConsumption();
            String customerRow1 = importPage.getCustomerNameConsumptionFromRow("1");
            String customerRow2 = importPage.getCustomerNameConsumptionFromRow("2");
            String customerRow3 = importPage.getCustomerNameConsumptionFromRow("3");
            String customerRow4 = importPage.getCustomerNameConsumptionFromRow("4");
            String customerRow5 = importPage.getCustomerNameConsumptionFromRow("5");
            softAssert.assertTrue(customerRow1.contains("Test 10"),"Customer Name in row 1 data is not as expected");
            softAssert.assertTrue(customerRow2.contains("Test 20"),"Customer Name in row 2 data is not as expected");
            softAssert.assertTrue(customerRow3.contains("Test 30"),"Customer Name in row 3 data is not as expected");
            softAssert.assertTrue(customerRow4.contains("Test 40"),"Customer Name in row 4 data is not as expected");
            softAssert.assertTrue(customerRow5.contains("Test 50"),"Customer Name in row 5 data is not as expected");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        ConfigurePage configurePage = new ConfigurePage(getDriver());
        configurePage.openConfigurePage();
        configurePage.openCSVVendorsTab();
        try {
            boolean result = configurePage.deleteCSVVendor(vendorName);
            softAssert.assertFalse(result,"Vendor is not deleted");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        softAssert.assertAll();
    }

    @Test
    public void verifyNonNumericChargeCheckboxChecked(){
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String vendorName = timeStamp+" Vendor";
        String currentUserDir = System.getProperty("user.dir");
        String csv1Consumption = currentUserDir + File.separator + csv3ConsumptionName;
        SoftAssert softAssert = new SoftAssert();
        loginToConnect();
        ImportPage importPage = new ImportPage(getDriver());
        importPage.openImportsPage();
        importPage.selectImportFile(csv1Consumption);
        try {
            importPage.createVendorConsumptionBased(vendorName);
            importPage.clickImportDataBtn();
            importPage.clickConvertNonNumericChargeToZeroCheckBox();
            importPage.mapDataForConsumptionBasedVendor();
            importPage.clickPreviewImportBtn();
            importPage.clickSaveAndContinueBtn();
            String chargeRow1 = importPage.getChargesInEachRowInDataPreview("1");
            String chargeRow2 = importPage.getChargesInEachRowInDataPreview("2");
            String chargeRow3 = importPage.getChargesInEachRowInDataPreview("3");
            String chargeRow4 = importPage.getChargesInEachRowInDataPreview("4");
            String chargeRow5 = importPage.getChargesInEachRowInDataPreview("5");
            softAssert.assertTrue(chargeRow1.contains("100.00"),"Charge in row 1 is wrong");
            softAssert.assertTrue(chargeRow2.contains("0.00"),"Charge in row 2 is wrong");
            softAssert.assertTrue(chargeRow3.contains("0.00"),"Charge in row 3 is wrong");
            softAssert.assertTrue(chargeRow4.contains("0.00"),"Charge in row 4 is wrong");
            softAssert.assertTrue(chargeRow5.contains("500.00"),"Charge in row 5 is wrong");
            importPage.clickImportDataBtn();
            importPage.verifyImportedDataConsumption();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        ConfigurePage configurePage = new ConfigurePage(getDriver());
        configurePage.openConfigurePage();
        configurePage.openCSVVendorsTab();
        try {
            boolean result = configurePage.deleteCSVVendor(vendorName);
            softAssert.assertFalse(result,"Vendor is not deleted");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        softAssert.assertAll();
    }

    @Test
    public void verifyNonNumericChargeCheckBoxUnchecked(){
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String vendorName = timeStamp+" Vendor";
        String currentUserDir = System.getProperty("user.dir");
        String csv1Consumption = currentUserDir + File.separator + csv3ConsumptionName;
        SoftAssert softAssert = new SoftAssert();
        loginToConnect();
        ImportPage importPage = new ImportPage(getDriver());
        importPage.openImportsPage();
        importPage.selectImportFile(csv1Consumption);
        try {
            importPage.createVendorConsumptionBased(vendorName);
            importPage.clickImportDataBtn();
            importPage.mapDataForConsumptionBasedVendor();
            importPage.clickPreviewImportBtn();
            String errorMsg = importPage.getErrorMsg();
            softAssert.assertTrue(errorMsg.contains("An error occurred: The column mapped to the " +
                    "charges contains non-numeric value. Please review your selected mapping and try again."));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        softAssert.assertAll();
    }

    @Test
    public void verifyMarkupIsAppliedForAllProducts(){
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String vendorName = timeStamp+" Vendor";
        String currentUserDir = System.getProperty("user.dir");
        String csv1Consumption = currentUserDir + File.separator + csv1ConsumptionName;
        SoftAssert softAssert = new SoftAssert();
        loginToConnect();
        ImportPage importPage = new ImportPage(getDriver());
        importPage.openImportsPage();
        try {
            importPage.importDataForNewVendorConsumptionBasedWithMarkup(csv1Consumption,vendorName,"20");
            String chargeRow1 = importPage.getBillablePriceInEachRowInDataPreview("1");
            String chargeRow2 = importPage.getBillablePriceInEachRowInDataPreview("2");
            String chargeRow3 = importPage.getBillablePriceInEachRowInDataPreview("3");
            String chargeRow4 = importPage.getBillablePriceInEachRowInDataPreview("4");
            String chargeRow5 = importPage.getBillablePriceInEachRowInDataPreview("5");
            softAssert.assertTrue(chargeRow1.contains("120.00"),"Billable Price in row 1 is wrong");
            softAssert.assertTrue(chargeRow2.contains("240.00"),"Billable Price in row 2 is wrong");
            softAssert.assertTrue(chargeRow3.contains("360.00"),"Billable Price in row 3 is wrong");
            softAssert.assertTrue(chargeRow4.contains("480.00"),"Billable Price in row 4 is wrong");
            softAssert.assertTrue(chargeRow5.contains("600.00"),"Billable Price in row 5 is wrong");
            importPage.clickImportDataBtn();
            importPage.verifyImportedDataConsumption();
            String customerRow1 = importPage.getCustomerNameConsumptionFromRow("1");
            String customerRow2 = importPage.getCustomerNameConsumptionFromRow("2");
            String customerRow3 = importPage.getCustomerNameConsumptionFromRow("3");
            String customerRow4 = importPage.getCustomerNameConsumptionFromRow("4");
            String customerRow5 = importPage.getCustomerNameConsumptionFromRow("5");
            softAssert.assertTrue(customerRow1.contains("Test 1"),"Customer Name in row 1 data is not as expected");
            softAssert.assertTrue(customerRow2.contains("Test 2"),"Customer Name in row 2 data is not as expected");
            softAssert.assertTrue(customerRow3.contains("Test 3"),"Customer Name in row 3 data is not as expected");
            softAssert.assertTrue(customerRow4.contains("Test 4"),"Customer Name in row 4 data is not as expected");
            softAssert.assertTrue(customerRow5.contains("Test 5"),"Customer Name in row 5 data is not as expected");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        ConfigurePage configurePage = new ConfigurePage(getDriver());
        configurePage.openConfigurePage();
        configurePage.openCSVVendorsTab();
        try {
            boolean result = configurePage.deleteCSVVendor(vendorName);
            softAssert.assertFalse(result,"Vendor is not deleted");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        softAssert.assertAll();
    }

    @Test
    public void verifyMarkupIsZeroForAllProducts(){
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String vendorName = timeStamp+" Vendor";
        String currentUserDir = System.getProperty("user.dir");
        String csv1Consumption = currentUserDir + File.separator + csv1ConsumptionName;
        SoftAssert softAssert = new SoftAssert();
        loginToConnect();
        ImportPage importPage = new ImportPage(getDriver());
        importPage.openImportsPage();
        try {
            importPage.importDataForNewVendorConsumptionBasedWithMarkup(csv1Consumption,vendorName,"0");
            String chargeRow1 = importPage.getBillablePriceInEachRowInDataPreview("1");
            String chargeRow2 = importPage.getBillablePriceInEachRowInDataPreview("2");
            String chargeRow3 = importPage.getBillablePriceInEachRowInDataPreview("3");
            String chargeRow4 = importPage.getBillablePriceInEachRowInDataPreview("4");
            String chargeRow5 = importPage.getBillablePriceInEachRowInDataPreview("5");
            softAssert.assertTrue(chargeRow1.contains("100.00"),"Billable Price in row 1 is wrong");
            softAssert.assertTrue(chargeRow2.contains("200.00"),"Billable Price in row 2 is wrong");
            softAssert.assertTrue(chargeRow3.contains("300.00"),"Billable Price in row 3 is wrong");
            softAssert.assertTrue(chargeRow4.contains("400.00"),"Billable Price in row 4 is wrong");
            softAssert.assertTrue(chargeRow5.contains("500.00"),"Billable Price in row 5 is wrong");
            importPage.clickImportDataBtn();
            importPage.verifyImportedDataConsumption();
            String customerRow1 = importPage.getCustomerNameConsumptionFromRow("1");
            String customerRow2 = importPage.getCustomerNameConsumptionFromRow("2");
            String customerRow3 = importPage.getCustomerNameConsumptionFromRow("3");
            String customerRow4 = importPage.getCustomerNameConsumptionFromRow("4");
            String customerRow5 = importPage.getCustomerNameConsumptionFromRow("5");
            softAssert.assertTrue(customerRow1.contains("Test 1"),"Customer Name in row 1 data is not as expected");
            softAssert.assertTrue(customerRow2.contains("Test 2"),"Customer Name in row 2 data is not as expected");
            softAssert.assertTrue(customerRow3.contains("Test 3"),"Customer Name in row 3 data is not as expected");
            softAssert.assertTrue(customerRow4.contains("Test 4"),"Customer Name in row 4 data is not as expected");
            softAssert.assertTrue(customerRow5.contains("Test 5"),"Customer Name in row 5 data is not as expected");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        ConfigurePage configurePage = new ConfigurePage(getDriver());
        configurePage.openConfigurePage();
        configurePage.openCSVVendorsTab();
        try {
            boolean result = configurePage.deleteCSVVendor(vendorName);
            softAssert.assertFalse(result,"Vendor is not deleted");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        softAssert.assertAll();
    }

    @Test
    public void verifyMarkupIsAppliedForAllProductsBeforeImport(){
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String vendorName = timeStamp+" Vendor";
        String currentUserDir = System.getProperty("user.dir");
        String csv1Consumption = currentUserDir + File.separator + csv1ConsumptionName;
        SoftAssert softAssert = new SoftAssert();
        loginToConnect();
        ImportPage importPage = new ImportPage(getDriver());
        importPage.openImportsPage();
        try {
            importPage.importDataForNewVendorConsumptionBasedWithMarkup(csv1Consumption,vendorName,"20");
            String chargeRow1 = importPage.getBillablePriceInEachRowInDataPreview("1");
            String chargeRow2 = importPage.getBillablePriceInEachRowInDataPreview("2");
            String chargeRow3 = importPage.getBillablePriceInEachRowInDataPreview("3");
            String chargeRow4 = importPage.getBillablePriceInEachRowInDataPreview("4");
            String chargeRow5 = importPage.getBillablePriceInEachRowInDataPreview("5");
            softAssert.assertTrue(chargeRow1.contains("120.00"),"Billable Price in row 1 is wrong");
            softAssert.assertTrue(chargeRow2.contains("240.00"),"Billable Price in row 2 is wrong");
            softAssert.assertTrue(chargeRow3.contains("360.00"),"Billable Price in row 3 is wrong");
            softAssert.assertTrue(chargeRow4.contains("480.00"),"Billable Price in row 4 is wrong");
            softAssert.assertTrue(chargeRow5.contains("600.00"),"Billable Price in row 5 is wrong");
            importPage.clickImportDataBtn();
            importPage.verifyImportedDataConsumption();
            String customerRow1 = importPage.getCustomerNameConsumptionFromRow("1");
            String customerRow2 = importPage.getCustomerNameConsumptionFromRow("2");
            String customerRow3 = importPage.getCustomerNameConsumptionFromRow("3");
            String customerRow4 = importPage.getCustomerNameConsumptionFromRow("4");
            String customerRow5 = importPage.getCustomerNameConsumptionFromRow("5");
            softAssert.assertTrue(customerRow1.contains("Test 1"),"Customer Name in row 1 data is not as expected");
            softAssert.assertTrue(customerRow2.contains("Test 2"),"Customer Name in row 2 data is not as expected");
            softAssert.assertTrue(customerRow3.contains("Test 3"),"Customer Name in row 3 data is not as expected");
            softAssert.assertTrue(customerRow4.contains("Test 4"),"Customer Name in row 4 data is not as expected");
            softAssert.assertTrue(customerRow5.contains("Test 5"),"Customer Name in row 5 data is not as expected");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        ConfigurePage configurePage = new ConfigurePage(getDriver());
        configurePage.openConfigurePage();
        configurePage.openCSVVendorsTab();
        try {
            boolean result = configurePage.deleteCSVVendor(vendorName);
            softAssert.assertFalse(result,"Vendor is not deleted");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        softAssert.assertAll();
    }

    @Test
    public void verifyImportZeroValueChargesToNo(){
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String vendorName = timeStamp+" Vendor";
        String currentUserDir = System.getProperty("user.dir");
        String csv1Consumption = currentUserDir + File.separator + csv4ConsumptionName;
        SoftAssert softAssert = new SoftAssert();
        loginToConnect();
        ImportPage importPage = new ImportPage(getDriver());
        importPage.openImportsPage();
        try {
            importPage.importDataForNewVendorConsumptionBasedWithMarkup(csv1Consumption,vendorName,"0");
            String chargeRow1 = importPage.getBillablePriceInEachRowInDataPreview("1");
            String chargeRow2 = importPage.getBillablePriceInEachRowInDataPreview("2");
            String chargeRow3 = importPage.getBillablePriceInEachRowInDataPreview("3");
            String chargeRow4 = importPage.getBillablePriceInEachRowInDataPreview("4");
            String chargeRow5 = importPage.getBillablePriceInEachRowInDataPreview("5");
            softAssert.assertTrue(chargeRow1.contains("100.00"),"Billable Price in row 1 is wrong");
            softAssert.assertTrue(chargeRow2.contains("0.00"),"Billable Price in row 2 is wrong");
            softAssert.assertTrue(chargeRow3.contains("300.00"),"Billable Price in row 3 is wrong");
            softAssert.assertTrue(chargeRow4.contains("0.00"),"Billable Price in row 4 is wrong");
            softAssert.assertTrue(chargeRow5.contains("500.00"),"Billable Price in row 5 is wrong");
            importPage.clickImportDataBtn();
            importPage.verifyImportedDataConsumption();
            String customerRow1 = importPage.getCustomerNameConsumptionFromRow("1");
            String customerRow2 = importPage.getCustomerNameConsumptionFromRow("2");
            String customerRow3 = importPage.getCustomerNameConsumptionFromRow("3");
            softAssert.assertTrue(customerRow1.contains("Test 1"),"Customer Name in row 1 data is not as expected");
            softAssert.assertTrue(customerRow2.contains("Test 3"),"Customer Name in row 2 data is not as expected");
            softAssert.assertTrue(customerRow3.contains("Test 5"),"Customer Name in row 3 data is not as expected");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        ConfigurePage configurePage = new ConfigurePage(getDriver());
        configurePage.openConfigurePage();
        configurePage.openCSVVendorsTab();
        try {
            boolean result = configurePage.deleteCSVVendor(vendorName);
            softAssert.assertFalse(result,"Vendor is not deleted");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        softAssert.assertAll();
    }

    @Test
    public void verifyImportZeroValueChargesToYes(){
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String vendorName = timeStamp+" Vendor";
        String currentUserDir = System.getProperty("user.dir");
        String csv1Consumption = currentUserDir + File.separator + csv4ConsumptionName;
        SoftAssert softAssert = new SoftAssert();
        loginToConnect();
        ImportPage importPage = new ImportPage(getDriver());
        importPage.openImportsPage();
        try {
            importPage.selectImportFile(csv1Consumption);
            importPage.createVendorConsumptionBased(vendorName);
            importPage.clickImportDataBtn();
            importPage.mapDataForConsumptionBasedVendor();
            importPage.clickPreviewImportBtn();
            importPage.importZeroValueCharges();
            importPage.clickSaveAndContinueBtn();
            String chargeRow1 = importPage.getBillablePriceInEachRowInDataPreview("1");
            String chargeRow2 = importPage.getBillablePriceInEachRowInDataPreview("2");
            String chargeRow3 = importPage.getBillablePriceInEachRowInDataPreview("3");
            String chargeRow4 = importPage.getBillablePriceInEachRowInDataPreview("4");
            String chargeRow5 = importPage.getBillablePriceInEachRowInDataPreview("5");
            softAssert.assertTrue(chargeRow1.contains("100.00"),"Billable Price in row 1 is wrong");
            softAssert.assertTrue(chargeRow2.contains("0.00"),"Billable Price in row 2 is wrong");
            softAssert.assertTrue(chargeRow3.contains("300.00"),"Billable Price in row 3 is wrong");
            softAssert.assertTrue(chargeRow4.contains("0.00"),"Billable Price in row 4 is wrong");
            softAssert.assertTrue(chargeRow5.contains("500.00"),"Billable Price in row 5 is wrong");
            importPage.clickImportDataBtn();
            importPage.verifyImportedDataConsumption();
            String customerRow1 = importPage.getCustomerNameConsumptionFromRow("1");
            String customerRow2 = importPage.getCustomerNameConsumptionFromRow("2");
            String customerRow3 = importPage.getCustomerNameConsumptionFromRow("3");
            String customerRow4 = importPage.getCustomerNameConsumptionFromRow("4");
            String customerRow5 = importPage.getCustomerNameConsumptionFromRow("5");
            softAssert.assertTrue(customerRow1.contains("Test 1"),"Customer Name in row 1 data is not as expected");
            softAssert.assertTrue(customerRow2.contains("Test 2"),"Customer Name in row 2 data is not as expected");
            softAssert.assertTrue(customerRow3.contains("Test 3"),"Customer Name in row 3 data is not as expected");
            softAssert.assertTrue(customerRow4.contains("Test 4"),"Customer Name in row 4 data is not as expected");
            softAssert.assertTrue(customerRow5.contains("Test 5"),"Customer Name in row 5 data is not as expected");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        ConfigurePage configurePage = new ConfigurePage(getDriver());
        configurePage.openConfigurePage();
        configurePage.openCSVVendorsTab();
        try {
            boolean result = configurePage.deleteCSVVendor(vendorName);
            softAssert.assertFalse(result,"Vendor is not deleted");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        softAssert.assertAll();
    }

    @Test
    public void verifyConfigureIndividualProductMarkup(){
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String vendorName = timeStamp+" Vendor";
        String currentUserDir = System.getProperty("user.dir");
        String csv1Consumption = currentUserDir + File.separator + csv5ConsumptionName;
        SoftAssert softAssert = new SoftAssert();
        loginToConnect();
        ImportPage importPage = new ImportPage(getDriver());
        importPage.openImportsPage();
        try {
            importPage.selectImportFile(csv1Consumption);
            importPage.createVendorConsumptionBased(vendorName);
            importPage.clickImportDataBtn();
            importPage.mapDataForConsumptionBasedVendor();
            importPage.clickPreviewImportBtn();
            importPage.addMarkupCharges("10");
            importPage.openSetConfigureIndividualProductMarkup();
            importPage.addMarkupForAProduct("B","20");
            importPage.addMarkupForAProduct("C","30");
            importPage.clickSaveAndContinueBtn();
            String chargeRow1 = importPage.getBillablePriceInEachRowInDataPreview("1");
            String chargeRow2 = importPage.getBillablePriceInEachRowInDataPreview("2");
            String chargeRow3 = importPage.getBillablePriceInEachRowInDataPreview("3");
            String chargeRow4 = importPage.getBillablePriceInEachRowInDataPreview("4");
            String chargeRow5 = importPage.getBillablePriceInEachRowInDataPreview("5");
            softAssert.assertTrue(chargeRow1.contains("110.00"),"Billable Price in row 1 is wrong");
            softAssert.assertTrue(chargeRow2.contains("240.00"),"Billable Price in row 2 is wrong");
            softAssert.assertTrue(chargeRow3.contains("390.00"),"Billable Price in row 3 is wrong");
            softAssert.assertTrue(chargeRow4.contains("440.00"),"Billable Price in row 4 is wrong");
            softAssert.assertTrue(chargeRow5.contains("550.00"),"Billable Price in row 5 is wrong");
            importPage.clickImportDataBtn();
            importPage.verifyImportedDataConsumption();
            String customerRow1 = importPage.getCustomerNameConsumptionFromRow("1");
            String customerRow2 = importPage.getCustomerNameConsumptionFromRow("2");
            String customerRow3 = importPage.getCustomerNameConsumptionFromRow("3");
            String customerRow4 = importPage.getCustomerNameConsumptionFromRow("4");
            String customerRow5 = importPage.getCustomerNameConsumptionFromRow("5");
            softAssert.assertTrue(customerRow1.contains("Test 1"),"Customer Name in row 1 data is not as expected");
            softAssert.assertTrue(customerRow2.contains("Test 2"),"Customer Name in row 2 data is not as expected");
            softAssert.assertTrue(customerRow3.contains("Test 3"),"Customer Name in row 3 data is not as expected");
            softAssert.assertTrue(customerRow4.contains("Test 4"),"Customer Name in row 4 data is not as expected");
            softAssert.assertTrue(customerRow5.contains("Test 5"),"Customer Name in row 5 data is not as expected");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        ConfigurePage configurePage = new ConfigurePage(getDriver());
        configurePage.openConfigurePage();
        configurePage.openCSVVendorsTab();
        try {
            boolean result = configurePage.deleteCSVVendor(vendorName);
            softAssert.assertFalse(result,"Vendor is not deleted");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        softAssert.assertAll();
    }
}

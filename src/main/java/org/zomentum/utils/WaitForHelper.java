package org.zomentum.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class WaitForHelper {
    public WebDriver driver;

    public WaitForHelper(WebDriver driver) {
        this.driver = driver;
    }

    public void implicitWait()
    {
        driver.manage().timeouts().implicitlyWait(Integer.parseInt(PropertyReader.readItem("elementLoadTimeout")),TimeUnit.MILLISECONDS);
    }


    public WebElement presenceOfTheElement(final By elementIdentifier) {
        WebElement firstResult = new WebDriverWait(driver, Duration.ofSeconds(100))
                .until(ExpectedConditions.presenceOfElementLocated(elementIdentifier));
        return firstResult;
    }

    public WebElement elementToBeClickable(final By elementIdentifier){
        WebElement firstResult = new WebDriverWait(driver, Duration.ofSeconds(100))
                .until(ExpectedConditions.elementToBeClickable(elementIdentifier));
        return firstResult;
    }

    public void invisibilityOfElement(final By elementIdentifier){
        new WebDriverWait(driver, Duration.ofSeconds(100))
                .until(ExpectedConditions.invisibilityOfElementLocated(elementIdentifier));
    }
}

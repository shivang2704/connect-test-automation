package org.zomentum.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.zomentum.base.BasePage;

public class ConfigurePage extends BasePage {

    WebDriver driver;

    public ConfigurePage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    By configureMenuItem = By.xpath("//span[text()='Configure']");
    By configurePageHeading = By.xpath("//h1[text()='Configuration']");
    By csvVendorTab = By.xpath("//li//a[text()='CSV Vendors']");
    String deleteVendorBtn = "//td/a[text()='%s']/ancestor::tr//td//button[@type='submit' and contains" +
            "(@class,'delete_vendor')]";
    By confirmDeleteBtn = By.xpath("//button[@type='button' and text()='Yes']");

    public void openConfigurePage(){
        waitForElementToAppear(configureMenuItem);
        waitForElementToBeClickable(configureMenuItem);
        click(configureMenuItem);
        waitForElementToAppear(configurePageHeading);
    }

    public void openCSVVendorsTab(){
        waitForElementToBeClickable(csvVendorTab);
        click(csvVendorTab);
    }

    public boolean deleteCSVVendor(String vendorName) throws InterruptedException {
        waitForElementToAppear(By.xpath(String.format(deleteVendorBtn,vendorName)));
        waitForElementToBeClickable(By.xpath(String.format(deleteVendorBtn,vendorName)));
        makeElementVisible(By.xpath(String.format(deleteVendorBtn,vendorName)));
        click(By.xpath(String.format(deleteVendorBtn,vendorName)));
        hardWait(2000);
        waitForElementToBeClickable(confirmDeleteBtn);
        click(confirmDeleteBtn);
        hardWait(3000);
        boolean result = elementIsDisplayed(By.xpath(String.format(deleteVendorBtn,vendorName)));
        return result;
    }

}

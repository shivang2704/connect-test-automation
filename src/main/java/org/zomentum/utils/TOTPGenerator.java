package org.zomentum.utils;

import org.jboss.aerogear.security.otp.Totp;

public class TOTPGenerator {

    public static PropertyReader props = new PropertyReader();
    public static String totpSecretCode = props.readItem("totpSecretCode");

    public static String getTwoFactorCode(){
        Totp totp = new Totp(totpSecretCode); // 2FA secret key
        String twoFactorCode = totp.now(); //Generated 2FA code here
        return twoFactorCode;
    }

}

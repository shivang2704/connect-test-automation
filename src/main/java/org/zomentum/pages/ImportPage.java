package org.zomentum.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.zomentum.base.BasePage;
import org.zomentum.utils.PropertyReader;

public class ImportPage extends BasePage {

    WebDriver driver;

    public ImportPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public static PropertyReader props = new PropertyReader();

    //Page Objects
    By importMenuItem = By.xpath("//span[text()='Import']");
    By importPageHeading = By.xpath("//h1[text()='Import data']");
    By chooseFileImportBtn = By.id("csv_file");
    By createVendorTabBtn = By.xpath("//a[text()='Create a vendor']");
    By existingVendorTabBtn = By.xpath("//a[text()='Use existing vendor']");
    By vendorTypeDD = By.xpath("//select[@id='type']//parent::div");
    By vendorNameTextBox = By.xpath("//input[@name='name']");
    By importDataBtn = By.id("submit");
    By mappingPageHeading = By.xpath("//h1[contains(text(),'Import data for')]");
    String customerNameCheckBox = "//input[@value='customer_name' and @name='mapping[%s][0]']";
    String customerIdCheckBox = "//input[@value='customer_id' and @name='mapping[%s][1]']";
    String productNameCheckBox = "//input[@value='product_name' and @name='mapping[%s][2]']";
    String productSKUCheckBox = "//input[@value='product_id' and @name='mapping[%s][3]']";
    String licenseCountCheckBox = "//input[@value='licenses' and @name='mapping[%s][4]']";
    String chargesCheckBox = "//input[@value='charge' and @name='mapping[%s][4]']";
    By previewImportBtn = By.xpath("(//button[@type='submit'])[2]");
    By saveAndContinueBtn = By.xpath("(//button[@type='submit'])[1]");
    By viewImportedDataBtn = By.cssSelector(".text-s");
    By contractsHeading = By.xpath("//h1[contains(text(),'contracts')]");
    By overviewHeading = By.xpath("//div[@class='col-span-6 row-start-1']");
    By existingVendorDD = By.xpath("//input[@id='vendor-selectized']/parent::div");
    By existingVendorTextBox = By.id("vendor-selectized");
    By licenseFirstRowContract = By.xpath("//table[@class='w-full']//tr[1]/td[8]");
    By licenseSecondRowContract = By.xpath("//table[@class='w-full']//tr[2]/td[8]");
    By licenseThirdRowContract = By.xpath("//table[@class='w-full']//tr[3]/td[8]");
    By licenseFourthRowContract = By.xpath("//table[@class='w-full']//tr[4]/td[8]");
    By licenseFifthRowContract = By.xpath("//table[@class='w-full']//tr[5]/td[8]");
    String customerNameContract = "//table[@class='w-full']//tr[%s]/td[2]//a";
    String customerNameConsumptionContract = "//table[@class='w-full']/tbody/tr[%s]/td[1]";
    By licenseCountRoundOffCheckBox = By.id("round_license_count_up");
    String disabledLicenseCountCheckBox = "//input[@value='licenses' and @name='mapping[%s][4]']/parent::" +
            "label[@class='disabled']";
    By configureDefaultColumnValueBtn = By.id("show");
    By defaultCustomerNameTextBox = By.id("customer_name");
    By defaultProductNameTextBox = By.id("product_name");
    By convertNonNumericChargeToZeroCheckBox = By.id("convert_non_numeric_charge_value_to_zero");
    String chargeInRow = "//tbody/tr[%s]/td[5]";
    String billablePriceInRow = "//tbody/tr[%s]/td[7]";
    By errorMsg = By.xpath("//p[contains(text(),'An error occurred:')]");
    By markupTextBox = By.id("markup");
    By importZeroChargesYes = By.id("include_zero_charges-1");
    By configureIndividualProductMarkupBtn = By.id("show");
    String defaultMarkupForEachProductBtn = "//td[text()='%s']/following-sibling::td//span[text()='default']";
    String defaultMarkupForEachProductTextBox = "//td[text()='%s']/following-sibling::td//input";



    //Page Actions
    public void openImportsPage(){
        waitForElementToAppear(importMenuItem);
        waitForElementToBeClickable(importMenuItem);
        click(importMenuItem);
        waitForElementToAppear(importPageHeading);
    }

    public void openSetConfigureIndividualProductMarkup(){
        waitForElementToAppear(configureIndividualProductMarkupBtn);
        waitForElementToBeClickable(configureIndividualProductMarkupBtn);
        click(configureIndividualProductMarkupBtn);
    }

    public void addMarkupForAProduct(String productName,String markup){
        waitForElementToAppear(By.xpath(String.format(defaultMarkupForEachProductBtn,productName)));
        waitForElementToBeClickable(By.xpath(String.format(defaultMarkupForEachProductBtn,productName)));
        click(By.xpath(String.format(defaultMarkupForEachProductBtn,productName)));
        waitForElementToAppear(By.xpath(String.format(defaultMarkupForEachProductTextBox,productName)));
        waitForElementToBeClickable(By.xpath(String.format(defaultMarkupForEachProductTextBox,productName)));
        click(By.xpath(String.format(defaultMarkupForEachProductTextBox,productName)));
        clearText(By.xpath(String.format(defaultMarkupForEachProductTextBox,productName)));
        writeText(By.xpath(String.format(defaultMarkupForEachProductTextBox,productName)),markup);
    }

    public void selectImportFile(String filePath){
        waitForElementToAppear(chooseFileImportBtn);
        driver.findElement(chooseFileImportBtn).sendKeys(filePath);
    }

    public void createVendorLicenseBased(String vendorName){
        waitForElementToAppear(createVendorTabBtn);
        waitForElementToBeClickable(createVendorTabBtn);
        click(createVendorTabBtn);
        waitForElementToBeClickable(vendorTypeDD);
        waitForElementToBeClickable(vendorNameTextBox);
        click(vendorNameTextBox);
        writeText(vendorNameTextBox,vendorName);
    }

    public void createVendorConsumptionBased(String vendorName) throws InterruptedException {
        waitForElementToAppear(createVendorTabBtn);
        waitForElementToBeClickable(createVendorTabBtn);
        makeElementVisible(createVendorTabBtn);
        click(createVendorTabBtn);
        waitForElementToBeClickable(vendorTypeDD);
        makeElementVisible(vendorTypeDD);
        selectFromDDByValue(By.id("type"),"consumption");
        //pressDownKeyAndEnter(By.id("type"));
        waitForElementToBeClickable(vendorNameTextBox);
        click(vendorNameTextBox);
        writeText(vendorNameTextBox,vendorName);
    }

    public void createVendorLicenseNotRoundOff(String vendorName){
        waitForElementToAppear(createVendorTabBtn);
        waitForElementToBeClickable(createVendorTabBtn);
        click(createVendorTabBtn);
        waitForElementToBeClickable(vendorTypeDD);
        waitForElementToBeClickable(vendorNameTextBox);
        click(vendorNameTextBox);
        writeText(vendorNameTextBox,vendorName);
        waitForElementToBeClickable(licenseCountRoundOffCheckBox);
        click(licenseCountRoundOffCheckBox);
    }

    public void selectVendorFromDD(String vendorName){
        waitForElementToAppear(existingVendorTabBtn);
        waitForElementToBeClickable(existingVendorTabBtn);
        click(existingVendorTabBtn);
        waitForElementToAppear(existingVendorDD);
        waitForElementToBeClickable(existingVendorDD);
        click(existingVendorDD);
        writeText(existingVendorTextBox,vendorName);
        pressEnter(existingVendorTextBox);
    }

    public void clickImportDataBtn(){
        waitForElementToBeClickable(importDataBtn);
        click(importDataBtn);
    }

    public void mapData1() throws InterruptedException {
        waitForElementToAppear(mappingPageHeading);
        waitForElementToAppear(By.xpath(String.format(customerNameCheckBox,0)));
        waitForElementToBeClickable(By.xpath(String.format(customerNameCheckBox,0)));
        makeElementVisible(By.xpath(String.format(customerNameCheckBox,0)));
        click(By.xpath(String.format(customerNameCheckBox,0)));
        waitForElementToBeClickable(By.xpath(String.format(customerIdCheckBox,1)));
        makeElementVisible(By.xpath(String.format(customerIdCheckBox,1)));
        click(By.xpath(String.format(customerIdCheckBox,1)));
        waitForElementToBeClickable(By.xpath(String.format(licenseCountCheckBox,2)));
        makeElementVisible(By.xpath(String.format(licenseCountCheckBox,2)));
        click(By.xpath(String.format(licenseCountCheckBox,2)));
        waitForElementToBeClickable(By.xpath(String.format(productNameCheckBox,3)));
        makeElementVisible(By.xpath(String.format(productNameCheckBox,3)));
        click(By.xpath(String.format(productNameCheckBox,3)));
        waitForElementToBeClickable(By.xpath(String.format(productSKUCheckBox,3)));
        click(By.xpath(String.format(productSKUCheckBox,3)));
    }

    public boolean nonRoundOffMapData() throws InterruptedException {
        waitForElementToAppear(mappingPageHeading);
        waitForElementToAppear(By.xpath(String.format(customerNameCheckBox,0)));
        waitForElementToBeClickable(By.xpath(String.format(customerNameCheckBox,0)));
        makeElementVisible(By.xpath(String.format(customerNameCheckBox,0)));
        click(By.xpath(String.format(customerNameCheckBox,0)));
        waitForElementToBeClickable(By.xpath(String.format(customerIdCheckBox,1)));
        makeElementVisible(By.xpath(String.format(customerIdCheckBox,1)));
        click(By.xpath(String.format(customerIdCheckBox,1)));
        boolean result = elementIsDisplayed(By.xpath(String.format(disabledLicenseCountCheckBox,2)));
        return result;
    }

    public String contractLicenseRow1(){
        waitForElementToAppear(licenseFirstRowContract);
        String result = readText(licenseFirstRowContract);
        return result;
    }

    public String contractLicenseRow2(){
        waitForElementToAppear(licenseSecondRowContract);
        String result = readText(licenseSecondRowContract);
        return result;
    }

    public String contractLicenseRow3(){
        waitForElementToAppear(licenseThirdRowContract);
        String result = readText(licenseThirdRowContract);
        return result;
    }

    public String contractLicenseRow4(){
        waitForElementToAppear(licenseFourthRowContract);
        String result = readText(licenseFourthRowContract);
        return result;
    }

    public String contractLicenseRow5(){
        waitForElementToAppear(licenseFifthRowContract);
        String result = readText(licenseFifthRowContract);
        return result;
    }

    public String getCustomerNameFromRow(String rowNumber){
        waitForElementToAppear(By.xpath(String.format(customerNameContract,rowNumber)));
        String result = readText(By.xpath(String.format(customerNameContract,rowNumber)));
        return result;
    }

    public String getCustomerNameConsumptionFromRow(String rowNumber){
        waitForElementToAppear(By.xpath(String.format(customerNameConsumptionContract,rowNumber)));
        String result = readText(By.xpath(String.format(customerNameConsumptionContract,rowNumber)));
        return result;
    }

    public void clickPreviewImportBtn() throws InterruptedException {
        waitForElementToBeClickable(previewImportBtn);
        makeElementVisible(previewImportBtn);
        click(previewImportBtn);
    }

    public void clickSaveAndContinueBtn() throws InterruptedException {
        waitForElementToBeClickable(saveAndContinueBtn);
        makeElementVisible(saveAndContinueBtn);
        click(saveAndContinueBtn);
    }

    public void openConfigureDefaultColumnValue(){
        waitForElementToAppear(mappingPageHeading);
        waitForElementToAppear(configureDefaultColumnValueBtn);
        waitForElementToBeClickable(configureDefaultColumnValueBtn);
        click(configureDefaultColumnValueBtn);
    }

    public void changeCustomerNameDefaultValue(String customerNameDefault){
        waitForElementToAppear(defaultCustomerNameTextBox);
        writeText(defaultCustomerNameTextBox,customerNameDefault);
    }

    public void changeProductNameDefaultValue(String productNameDefault){
        waitForElementToAppear(defaultProductNameTextBox);
        writeText(defaultProductNameTextBox,productNameDefault);
    }

    public void mapData2() throws InterruptedException {
        waitForElementToAppear(mappingPageHeading);
        waitForElementToAppear(By.xpath(String.format(customerIdCheckBox,0)));
        waitForElementToBeClickable(By.xpath(String.format(customerIdCheckBox,0)));
        makeElementVisible(By.xpath(String.format(customerIdCheckBox,0)));
        click(By.xpath(String.format(customerIdCheckBox,0)));
        waitForElementToBeClickable(By.xpath(String.format(licenseCountCheckBox,1)));
        makeElementVisible(By.xpath(String.format(licenseCountCheckBox,1)));
        click(By.xpath(String.format(licenseCountCheckBox,1)));
        waitForElementToBeClickable(By.xpath(String.format(productSKUCheckBox,2)));
        makeElementVisible(By.xpath(String.format(productSKUCheckBox,2)));
        click(By.xpath(String.format(productSKUCheckBox,2)));
    }

    public void mapDataForConsumptionBasedVendor() throws InterruptedException {
        waitForElementToAppear(mappingPageHeading);
        waitForElementToAppear(By.xpath(String.format(customerNameCheckBox,0)));
        waitForElementToBeClickable(By.xpath(String.format(customerNameCheckBox,0)));
        makeElementVisible(By.xpath(String.format(customerNameCheckBox,0)));
        click(By.xpath(String.format(customerNameCheckBox,0)));
        waitForElementToBeClickable(By.xpath(String.format(customerIdCheckBox,1)));
        makeElementVisible(By.xpath(String.format(customerIdCheckBox,1)));
        click(By.xpath(String.format(customerIdCheckBox,1)));
        waitForElementToBeClickable(By.xpath(String.format(chargesCheckBox,2)));
        makeElementVisible(By.xpath(String.format(chargesCheckBox,2)));
        click(By.xpath(String.format(chargesCheckBox,2)));
        waitForElementToBeClickable(By.xpath(String.format(productNameCheckBox,3)));
        makeElementVisible(By.xpath(String.format(productNameCheckBox,3)));
        click(By.xpath(String.format(productNameCheckBox,3)));
        waitForElementToBeClickable(By.xpath(String.format(productSKUCheckBox,3)));
        click(By.xpath(String.format(productSKUCheckBox,3)));
    }

    public void clickConvertNonNumericChargeToZeroCheckBox(){
        waitForElementToAppear(convertNonNumericChargeToZeroCheckBox);
        waitForElementToBeClickable(convertNonNumericChargeToZeroCheckBox);
        click(convertNonNumericChargeToZeroCheckBox);
    }

    public String getChargesInEachRowInDataPreview(String rowNumber){
        waitForElementToAppear(By.xpath(String.format(chargeInRow,rowNumber)));
        String result = readText(By.xpath(String.format(chargeInRow,rowNumber)));
        return result;
    }

    public String getBillablePriceInEachRowInDataPreview(String rowNumber){
        waitForElementToAppear(By.xpath(String.format(billablePriceInRow,rowNumber)));
        String result = readText(By.xpath(String.format(billablePriceInRow,rowNumber)));
        return result;
    }

    public String getErrorMsg(){
        waitForElementToAppear(errorMsg);
        String result = readText(errorMsg);
        return result;
    }

    public void verifyImportedData(){
        waitForElementToAppear(viewImportedDataBtn);
        waitForElementToBeClickable(viewImportedDataBtn);
        click(viewImportedDataBtn);
        waitForElementToAppear(contractsHeading);
    }

    public void verifyImportedDataConsumption(){
        waitForElementToAppear(viewImportedDataBtn);
        waitForElementToBeClickable(viewImportedDataBtn);
        click(viewImportedDataBtn);
        waitForElementToAppear(overviewHeading);
    }

    public void addMarkupCharges(String markup){
        waitForElementToAppear(markupTextBox);
        clearText(markupTextBox);
        writeText(markupTextBox, markup);
    }

    public void importZeroValueCharges(){
        waitForElementToAppear(importZeroChargesYes);
        waitForElementToBeClickable(importZeroChargesYes);
        click(importZeroChargesYes);
    }

    public void importDataForNewVendorLicenseBased(String filePath, String vendorName) throws InterruptedException {
        selectImportFile(filePath);
        createVendorLicenseBased(vendorName);
        clickImportDataBtn();
        mapData1();
        clickPreviewImportBtn();
        clickImportDataBtn();
    }

    public boolean importDataForNonRoundOffLicense(String filePath, String vendorName) throws InterruptedException {
        selectImportFile(filePath);
        createVendorLicenseNotRoundOff(vendorName);
        clickImportDataBtn();
        boolean result = nonRoundOffMapData();
        return result;
    }

    public void updateImportedCSV(String filePath,String vendorName) throws InterruptedException {
        selectImportFile(filePath);
        selectVendorFromDD(vendorName);
        clickImportDataBtn();
        clickPreviewImportBtn();
        clickImportDataBtn();
    }

    public void importDataForNewVendorConsumptionBased(String filePath, String vendorName) throws InterruptedException {
        selectImportFile(filePath);
        createVendorConsumptionBased(vendorName);
        clickImportDataBtn();
        mapDataForConsumptionBasedVendor();
        clickPreviewImportBtn();
        clickSaveAndContinueBtn();
        clickImportDataBtn();
    }

    public void updateImportedCSVConsumption(String filePath, String vendorName) throws InterruptedException {
        selectImportFile(filePath);
        selectVendorFromDD(vendorName);
        clickImportDataBtn();
        clickPreviewImportBtn();
        clickSaveAndContinueBtn();
        clickImportDataBtn();
    }

    public void importDataForNewVendorConsumptionBasedWithMarkup(String filePath, String vendorName, String markup) throws InterruptedException {
        selectImportFile(filePath);
        createVendorConsumptionBased(vendorName);
        clickImportDataBtn();
        mapDataForConsumptionBasedVendor();
        clickPreviewImportBtn();
        addMarkupCharges(markup);
        clickSaveAndContinueBtn();
    }
}
